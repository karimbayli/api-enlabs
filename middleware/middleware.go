package middleware

import (
	"net/http"
	"strings"
	customHTTP "api-enlabs/http"
	"api-enlabs/utils"
)
var sourceTypeList = []string{"game","server","payment"}
func JWTMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("Authorization")
		if len(tokenString) == 0 {
			customHTTP.NewErrorResponse(w, http.StatusUnauthorized, "Authentication failure")
			return
		}
		tokenString = strings.Replace(tokenString, "Bearer ", "", 1)
		_, err := VerifyToken(tokenString)
		if err != nil {
			customHTTP.NewErrorResponse(w, http.StatusUnauthorized, "Error verifying JWT token: " + err.Error())
			return
		}

		sourceType := r.Header.Get("Source-Type")
		if !utils.Contains(sourceTypeList,sourceType){
			customHTTP.NewErrorResponse(w, http.StatusUnauthorized, "not permitted Source-Type")
		}else if strings.EqualFold(sourceType,"payment"){
			next.ServeHTTP(w, r)
		}else {
			customHTTP.NewErrorResponse(w, http.StatusNotImplemented, "not implemented yet")
		}
		//pass userId claim to req
		//todo: find a better way to convert the claim to string
/*		userId := strconv.FormatFloat(claims.(jwt.MapClaims)["userId"].(float64), 'g', 1, 64)
		r.Header.Set("userId", userId)*/

	})
}