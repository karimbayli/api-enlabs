package models

import (
	"api-enlabs/db"
	customHTTP "api-enlabs/http"
	"api-enlabs/utils"
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	var users []User
	//since we're passing a pointer to users, db.Find assigns array to the address
	db.DB.Find(&users)
	if len(users)==0{
		customHTTP.NewErrorResponse(w, http.StatusOK, errors.New("empty user list").Error())
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(users)
}

func ShowHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var user User
	db.DB.First(&user, params["userId"])
	if user.Id==0{
		customHTTP.NewErrorResponse(w, http.StatusOK, errors.New("user_id: " + params["userId"]+" doesnt exists").Error())
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(user)
}



func CreateTransaction(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["userId"])
	if  err!=nil{
		utils.RespondErr(w, err)
		return
	}
	transactionDetail := &Transaction{}
	if err := utils.ReadRequest(r, transactionDetail); err != nil {
		customHTTP.NewErrorResponse(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	errV := transactionDetail.Validate()
	if  errV!=nil{
		customHTTP.NewErrorResponse(w, http.StatusUnprocessableEntity, errV.Error())
		return
	}
	errC := transactionDetail.Create(id)
	if errC!=nil{
		customHTTP.NewErrorResponse(w, http.StatusInternalServerError, errC.Error())
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(&transactionDetail)

}


