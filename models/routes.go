package models

import "api-enlabs/router"

var Routes = router.RoutePrefix{
	"/api",
	[]router.Route{
		router.Route{
			"UsersIndex",
			"GET",
			"/user/list",
			IndexHandler,
			false,
		},
		router.Route{
			"UsersShow",
			"GET",
			"/user/{userId}",
			ShowHandler,
			false,
		},
		router.Route{
			"PaymentCreate",
			"POST",
			"/user/{userId}/payment",
			CreateTransaction,
			true,
		},/*
		router.Route{
			"PaymentCreate",
			"GET",
			"/transactions",
			ShowHandlerTransactions,
			false,
		},*/
	},
}