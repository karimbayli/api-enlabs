package models

import (
	"errors"
	"api-enlabs/utils"
)

type Transaction struct {
	Id   int `gorm:"primary_key" json:"-"`
	UserId        int     `json:"user_id" db:"user_id"`
	Amount        float64 `json:"amount" db:"amount"`
	ClientTransactionID string  `json:"client_transaction_id" db:"client_transaction_id"`
	State         string  `json:"state" db:"state"`
	Status        string  `json:"status" db:"status"`
	//CreatedAt     string  `json:"created_at" db:"created_at"`
}


var PaymentStateList = []string{"win", "lost"}

func (transaction *Transaction) Validate() ( error) {

	if !utils.Contains(PaymentStateList, transaction.State) {
		return errors.New("payment state should be [win] || [lost]")
	}
	if transaction.Amount < 0 {
		return errors.New("amount should be more than 0 [non-negative]")
	}

	if transaction.ClientTransactionID == "" {
		return errors.New( "empty transaction id")
	}
	//All the required parameters are present
	return nil
}