package models

import (
	"github.com/dgrijalva/jwt-go"
	"os"
	"time"
)

type User struct {
	Id   int `gorm:"primary_key" json:"id"`
	Email string `gorm:"unique_index" json:"email"`
	Name string `json:"name"`
	Balance float64 `json:"balance"`
	UpdatedAt *time.Time `json:"-"`
	CreatedAt *time.Time  `json:"-"`
}

type JWTToken struct {
	Token string `json:"token"`
}


func (u User) generateJWT() (JWTToken, error) {
	signingKey := []byte(os.Getenv("JWT_SECRET"))
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"exp": time.Now().Add(time.Hour * 1 * 1).Unix(),
		"user_id": int(u.Id),
		"name": u.Name,
		"email": u.Email,
	})
	tokenString, err := token.SignedString(signingKey)
	return JWTToken{tokenString}, err
}