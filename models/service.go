package models

import (
	"api-enlabs/db"
	"errors"
)
func (transaction *Transaction) Create(id int) error {

	//var transactionNew Transaction
	//
	rows := db.DB.Where("client_transaction_id=?",transaction.ClientTransactionID).Find(&transaction).RowsAffected
	if rows>0{
		return errors.New("payment with this transaction id already exists")
	}


	var user User
	//
	db.DB.First(&user, id)
	if user.Id==0{
		return errors.New("user doestn exists")
	}

	switch transaction.State {
	case "win":
		user.Balance += transaction.Amount
	case "lost":
		if user.Balance < transaction.Amount{
			return errors.New("insufficient balance")
		}
		user.Balance -= transaction.Amount
	default:
		return errors.New("not valid state [win|lost]")
	}
	if err := transaction.TransactionProcess(&user); err != nil {
		return err
	}
	transaction.UserId = user.Id
	return  nil

}

func (transaction *Transaction) TransactionProcess(user *User) error {
	tx := db.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return err
	}
	if err := tx.Create(&Transaction{
		UserId:              user.Id,
		Amount:              transaction.Amount,
		ClientTransactionID: transaction.ClientTransactionID,
		State:               transaction.State,
		Status:              "approved",
	}).Error; err != nil {
		tx.Rollback()
		return err
	}
	//tx.Find(&user).Limit(5)
	if err := tx.Model(&user).Update("balance", user.Balance).Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error

}

func ProcessCanceledTransactions()  {
	var transactions []Transaction
	//db.DB.Limit(10).Last(&transactions)
	db.DB.Order("id desc").Limit(29).Find(&transactions)
	length := len(transactions)
	if len(transactions)>20{
		length = 20}
	if len(transactions)==0{
		return
	}
	var listOfCanceled []int
	for index:=0; index< length; index++ {
		if (index+1)%2!=0{
			//fmt.Printf("index %d, id: %d\n",index,transactions[index].Id)
			if transactions[index].Status!="canceled"{
				listOfCanceled = append(listOfCanceled, transactions[index].Id)
			}
		}
	}
	for _, v := range listOfCanceled {
		var user User
		var transaction Transaction
		//
		db.DB.First(&transaction, v)
		db.DB.First(&user,transaction.UserId)
		if transaction.State=="win"{
			user.Balance-=transaction.Amount
		} else {
			user.Balance+=transaction.Amount
		}
		db.DB.Model(&user).Update("balance", user.Balance)
		db.DB.Model(&transaction).Update("status", "canceled")
	}
	return
}
