package db

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
	"os"
)
var schema = `
create table if not exists  users
(	id serial not null
		constraint table_name_pk
			primary key,
	name text not null,
	email text not null,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	deleted_at TIMESTAMPTZ,
	updated_at TIMESTAMPTZ,
	balance double precision not null
);


create table if not exists transactions
(
	id serial
		constraint transaction_pk
			primary key,
	amount float not null,
	state text not null,
	client_transaction_id text,
	status text,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	deleted_at TIMESTAMPTZ,	
	updated_at TIMESTAMPTZ,
	user_id int
		constraint transaction_users_id_fk
			references users
				on update restrict on delete restrict
);`
//database global
var DB *gorm.DB

func SetupDB() *gorm.DB {

	//db config vars
	e := godotenv.Load()
	if e != nil {
		panic(e)
	}
	var dbHost string = os.Getenv("db_host")
	var dbName string = os.Getenv("db_name")
	var dbUser string = os.Getenv("db_user")
	var dbPassword string = os.Getenv("db_pass")
	//var dbPort string = os.Getenv("db_port")
	//var dbType string = os.Getenv("db_type")


	//connect to db
	db, dbError := gorm.Open("postgres", "host=" + dbHost +" user="+dbUser+ " password=" + dbPassword + " dbname="+dbName+" sslmode=disable")
	if dbError != nil {
		panic("Failed to connect to database")
	}
	_, err := db.DB().Query(schema)
	if err!=nil{
		panic(err)
	}

	db.DB().SetMaxIdleConns(0)

	return db
}
