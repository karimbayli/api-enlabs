package utils

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"enlabs-project/errors"
	"enlabs-project/render"
)

func Message(status bool, message string) (map[string]interface{}) {
	return map[string]interface{}{"status": status, "message": message}
}


func GenerateUUID() (string){
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%x-%x-%x-%x-%x",
		b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
}

func Contains(arr []string, str string) bool {
	for _, a := range arr {
		if a == str {
			return true
		}
	}
	return false
}

func handleError(err error) {
	if err != nil {
		log.Println(err)
	}
}

func Respond(wr http.ResponseWriter, status int, v interface{}) {
	if err := render.JSON(wr, status, v); err != nil {
		if loggable, ok := wr.(errorLogger); ok {
			loggable.Errorf("failed to write data to http ResponseWriter: %s", err)
		}
	}
}

func RespondErr(wr http.ResponseWriter, err error) {
	if e, ok := err.(*errors.Error); ok {
		Respond(wr, e.Code, e)
		return
	}
	Respond(wr, http.StatusInternalServerError, err.Error())
}

func ReadRequest(req *http.Request, v interface{}) error {
	if err := json.NewDecoder(req.Body).Decode(v); err != nil {
		return errors.Validation("Failed to read request body")
	}

	return nil
}

type errorLogger interface {
	Errorf(msg string, args ...interface{})
}