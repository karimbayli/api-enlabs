Your static JWT token:
Authorization:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJlbmxhYnMuc2UifQ.bQFbRKc41KTir0HmMH6xeCbALbt3DbP0Bv7L9nW3fQY

----------------------------------------------------------------------------------------
There are 4 test users(accounts),

You can list them by CURL:
curl -X GET   http://localhost:8000/api/user/list   -H 'Content-Type: application/json'
--------------------------------------------------------------------------------------
Process transaction:
{userId} = 3 [Javid K] account || {userId} = 4 [Enlabs Ab] account
curl -X POST \
  http://localhost:8000/api/user/{userId}/payment \
  -H 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJlbmxhYnMuc2UifQ.bQFbRKc41KTir0HmMH6xeCbALbt3DbP0Bv7L9nW3fQY' \
  -H 'Source-Type: payment' \
  -d '{
	"amount":2500,
	"state":"win",
	"client_transaction_id":"112sasdfddsdefs4ddsddfsdffffsd"
}'












