package main

import (
	"api-enlabs/models"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/joho/godotenv"
	"github.com/robfig/cron"
	"log"
	"net/http"
	"os"
	"api-enlabs/db"
)
func init(){
	e := godotenv.Load()
	if e != nil {
		panic(e)
	}
	fmt.Println("cron job")
	c := cron.New(cron.WithLogger(
		cron.VerbosePrintfLogger(log.New(os.Stdout, "cron: ", log.LstdFlags))))
	c.AddFunc("@every "+ os.Getenv("cron_time") +"m", models.ProcessCanceledTransactions)
	go c.Start()
}
func main(){

	//init router

	port := os.Getenv("server_port")
	router := NewRouter()

	//Setup database
	db.DB = db.SetupDB()
	defer db.DB.Close()

	//create http server
	loggedRouter := handlers.LoggingHandler(os.Stdout, router)
	log.Println(http.ListenAndServe(":"+port, loggedRouter))
}